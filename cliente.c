
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <iostream>

#define PORT 3490 // puerto al que vamos a conectar 

#define MAXBUFLEN 100  // Maximo de caracteres para el buffer
#define MAX 100

using namespace std;
    
char num[10],opcn[1];
int sockfd, numbytes;  
char buf[MAXBUFLEN];
struct hostent *he;
struct sockaddr_in their_addr; // información de la dirección de destino 
size_t len;

FILE *archivo;
FILE *archivo2;
char name[MAX],name2[MAX];;
char linea[MAX];
int cont_line;
char loop[10];

void crear_socket();
void menu();
void conectar();
void atributos();
void host_name(char *argv[]);
void enviar_opcion();
void enviar(char cadena[]);
void recibir();
void enviar_lineas();

void enviar_lineas()//enviar lineas del archivo, para que el servidor las procese
{
  cont_line=-1;//contador de lineas del archivo
  while(!feof(archivo))//recorro el archivo, hasta llegar al final de este
  {
    fgets(linea,100,archivo);//agarro cada linea del archivo
    cont_line++;//contador de lineas del archivo
  }
  
  sprintf(loop,"%d",cont_line);//paso el valor entero de cont_line a loop, que es de tipo char[10]
  enviar(loop);//envío la cantidad de lineas al servidor, osea: tamaño que va a durar la conversación entre el servidor y el cliente
  recibir();//++++++++++
  
  archivo=fopen(name,"r");//vuelvo a abrir el archivo pq el puntero quedó al final de este
 
  archivo2=fopen(name2,"w+");//abro el archivo donde voy a guardar el resultado
  
  while(!feof(archivo))//recorre todo el contenido del archivo
  {
    fgets(linea,100,archivo);//agarra toda una linea del txt con un maximo de 100 caracteres
    if(!feof(archivo))
    {
      enviar(linea);//envio las lineas al servidor
      recibir();//recibo las lineas ya modificadas por el servidor
      fprintf(archivo2,"%s",buf);//guardo en el archivo2 las lineas modificadas
    }
  }
  //se cierran los archivos
  fclose(archivo);
  fclose(archivo2);
}
  
void menu()
{
  int opc,opc2;
  
  printf("***************\n");
  printf("* 1. Archivos *\n");
  printf("* 2. Numeros  *\n");
  printf("* 3. Salir    *\n");
  printf("***************\n\n");
  scanf("%d",&opc);
  switch(opc)
  {
    case 1://archivos
      
    cout << "Escriba el nombre del archivo que desea abrir: ";
    cin >> name;//se guarda en name el nombre del archivo de texto
    strcpy(name2,name);
    strcat(name2,"2");
    strcat(name,".txt");//se añade ".txt" en la última línea de name, para establecer la extensión
    strcat(name2,".txt");//se añade ".txt" en la última línea de name, para establecer la extensión
    archivo=fopen(name,"r");//se abre el archivo con el modo "r"(solo lectura y debe existir previamente)
    
    cout << "1. InitCap" << endl;
    cout << "2. UpCap" << endl;
    cout << "3. LowCap" << endl;
    cout << "4. Volver" << endl;
    cin >> opc2;
  
    switch(opc2)
    {
      case 1://InitCap
	opcn[0]='1';
	enviar_opcion();// se envia al servidor el #1 indicandole que es InitCap
	recibir();//+++++++++++
	enviar_lineas();
	cout << "Listo!!! se han cambiado todas las primer letras a Mayuscula" << endl;
      break;
      
      case 2://UpCAp
	opcn[0]='2';
	enviar_opcion();// se envia al servidor el #2 indicandole que es UpCap
	recibir();//+++++++++++
	enviar_lineas();
	cout << "Listo!!! se han cambiado todas las letras a MAYUSCULA" << endl;
      break;
      
      case 3://LowCap
	opcn[0]='3';
	enviar_opcion();// se envia al servidor el #3 indicandole que es LowCap
	recibir();//+++++++++++
	enviar_lineas();
	cout << "Listo!!! se han cambiado todas las letras a minuscula" << endl;
      break;
      
      case 4://Volver

      break;
    }
    break;
    
    case 2://numeros
    opcn[0]='4';
    cout << "Digite un numero: ";
    cin >> num;
    enviar_opcion();// se envia al servidor el #4 indicandole que es la conversion de un numero a letras
    recibir();// hago esto pq cuando hacia 2 send, el servidor me los recivia con 1 recv, y necesitaba que estuvieran separados
    enviar(num);// se envia el numero que se quiere averiguar su correspondiente en letras
    recibir();// se recive la respuesta
    printf("Recivido: %s\n",buf);// Visualizamos lo recibido
    break;
    
    case 3://salir
      exit(1);
    break;
  }
}

void host_name(char *argv[])//Se utiliza para convertir un nombre de un host a su dirección IP
{
  if ((he=gethostbyname(argv[1])) == NULL)
  {  
    perror("gethostbyname");
    exit(1);
  }
}

void crear_socket()//se crea el socket para comunicarse con el servidor
{
  if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) 
  {
    perror("socket");
    exit(1);
  }	
}

void conectar()// nos conectamos con el servidor
{
  if (connect(sockfd, (struct sockaddr *)&their_addr,sizeof(struct sockaddr)) == -1) 
  {
    perror("connect");
    exit(1);
  }
}

void atributos()//definimos el tipo de transferencia,puerto y la ip con la que nos vamos a comunicar
{
  len = sizeof(struct sockaddr_in);
  their_addr.sin_family = AF_INET;    // Ordenación de bytes de la máquina 
  their_addr.sin_port = htons(PORT);  // short, Ordenación de bytes de la red 
  their_addr.sin_addr = *((struct in_addr *)he->h_addr);// se pasa la direccion ip al socket 
  memset(&(their_addr.sin_zero), 8, len);  // poner a cero el resto de la estructura 
}

void enviar_opcion()//el servidor recive que tipo de funcion tiene que realizar, las opciones son: 1 = InitCap ... 2 = UpCap ... 3 = LowCap ... 4 = Numero -> Letras
{
  if ((send(sockfd, opcn, strlen(opcn), 0)) == -1) 
  {
    perror("send_opc");
    exit(1);
  }
}

void enviar(char cadena[])//envio de datos
{
  if ((send(sockfd, cadena, strlen(cadena), 0)) == -1) 
  {
    perror("send");
    exit(1);
  }	  
}

void recibir()//resibir datos
{
  if ((numbytes=recv(sockfd, buf, MAXBUFLEN, 0)) == -1)
  {
    perror("recv");
    exit(1);
  }
}

int main(int argc, char *argv[])//metodo principal del programa
{
  if (argc != 2) 
  {
    fprintf(stderr,"usaste solo: %d argumento, escribe también el nombre del servidor\n",argc);
    exit(1);
  }
	
  host_name(argv);
	
  crear_socket();
  
  atributos();
  
  conectar();
  
  menu();

  close(sockfd);
  return 0;
} 
